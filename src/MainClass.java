import java.io.*;

/**
 * Created by Lilok on 3/8/2018.
 */
public class MainClass {

    private static volatile int progressCountForFileOne;
    private static volatile int progressCountForFileTwo;
    private static volatile int progressCountForFileThree;
    private final static Object lock = new Object();

    public static void main(String[] args) {

        File firstFile = new File("D:\\firstFile.txt");
        File secondFile = new File("D:\\secondFile.txt");
        File thirdFile = new File("D:\\thirdFile.txt");
        int firstThreadTime = 100;
        int secondThreadTime = 200;
        int thirdThreadTime = 300;
        MainClass.runFileWriteThread(firstThreadTime, firstFile);
        MainClass.runFileWriteThread(secondThreadTime, secondFile);
        MainClass.runFileWriteThread(thirdThreadTime, thirdFile);
        MainClass.startProgressReportThread();

    }


    private static void startProgressReportThread() {

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                int lastValueOfCountOne = 0;
                int lastValueOfCountTwo = 0;
                int lastValueOfCountThree = 0;
                while (progressCountForFileOne <= 100 || progressCountForFileTwo <= 100 || progressCountForFileThree <= 100) {
                    try {
                        Thread.sleep(1000 / 60);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (progressCountForFileOne != lastValueOfCountOne || progressCountForFileTwo != lastValueOfCountTwo ||
                            progressCountForFileThree != lastValueOfCountThree) {
                        lastValueOfCountOne = progressCountForFileOne;
                        lastValueOfCountTwo = progressCountForFileTwo;
                        lastValueOfCountThree = progressCountForFileThree;
                        printProgress(progressCountForFileOne, "FileOne ");
                        printProgress(progressCountForFileTwo, "FileTwo");
                        printProgress(progressCountForFileThree, "FileThree");
                        synchronized (lock) {
                            try {
                                lock.notify();
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        System.out.println();
                    }
                }
            }
        });
        th.start();
    }

    private static void printProgress(int progressCountFileNumber, String file) {

        System.out.print(file + progressCountFileNumber + "% |");
        for (int i = 0; 10 > i; i++) {
            System.out.print(i < progressCountFileNumber / 10 ? "#" : ".");
        }
        System.out.print("|\n");
    }


    private static void runFileWriteThread(int sleepTime, File file) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                FileWriter fw = null;
                try {
                    fw = new FileWriter(file);
                    for (int i = 0; i <= 100; i++) {
                        fw.write(i + ", " + "\n");
                        Thread.sleep(sleepTime);
                        // 0 check is removed from "if" statement below (i!=0)
                        if (i % 10 == 0) {
                            switch (file.getName()) {
                                case "firstFile.txt":
                                    progressCountForFileOne = i;
                                    break;
                                case "secondFile.txt":
                                    progressCountForFileTwo = i;
                                    break;
                                case "thirdFile.txt":
                                    progressCountForFileThree = i;
                                    break;
                            }
                            synchronized (lock) {
                                lock.notify();
                                lock.wait();
                            }
                        }
                    }
                } catch (IOException |
                        InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (fw != null) {
                            fw.close();
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }
}